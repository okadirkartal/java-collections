package com.kartalTest.collections.lists;

import com.kartal.collections.sets.ProductCatalogue;
import com.kartal.collections.sets.TreeProductCatalogue;
import org.junit.Test;

import static com.kartal.collections.enums.ProductFixtures.*;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;


public class ProductCatalogueTest {

    @Test
    public void shouldOnlyHoldUniqueProducts() throws Exception {
        ProductCatalogue catalogue = new ProductCatalogue();

        catalogue.isSuppliedBy(kadir);
        catalogue.isSuppliedBy(kartal);

        assertThat(catalogue, containsInAnyOrder(door, floorPanel, window));
    }

    @Test
    public void shouldFindLightVanProducts() throws Exception {
        TreeProductCatalogue catalogue = new TreeProductCatalogue();

        catalogue.isSuppliedBy(kadir);
        catalogue.isSuppliedBy(kartal);

        assertThat(catalogue.lightVanProducts(), containsInAnyOrder(window));
    }


    @Test
    public void shouldFindHeavyVanProducts() throws Exception {
        TreeProductCatalogue catalogue = new TreeProductCatalogue();

        catalogue.isSuppliedBy(kadir);
        catalogue.isSuppliedBy(kartal);

        assertThat(catalogue.heavyVanProducts(), containsInAnyOrder(door, floorPanel));
    }

}
