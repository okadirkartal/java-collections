package com.kartal.collections;

import com.kartal.collections.enums.ProductFixtures;
import com.kartal.collections.lists.Product;

import java.util.HashMap;
import java.util.Map;

public class Java8Enhancements {

    public static void Sample1() {
        final Product defaultPoduct = new Product(-1, "Whatever the customer wants", 100);

        final Map<Integer, Product> idToProducts = new HashMap<>();
        idToProducts.put(1, ProductFixtures.door);
        idToProducts.put(2, ProductFixtures.floorPanel);
        idToProducts.put(3, ProductFixtures.window);

        Product result = idToProducts.get(10);
        System.out.println(result);

        Product result2 = idToProducts.getOrDefault(10, defaultPoduct);
        System.out.println(result2);

        Product result3 = idToProducts.replace(1, new Product(1, "Big Door", 50));
        System.out.println(idToProducts.get(1));

        idToProducts.replaceAll((id, oldProdıct) ->
                new Product(id, oldProdıct.getName(), oldProdıct.getWeight() + 10));

        System.out.println(idToProducts);

        final Product result4 = idToProducts.computeIfAbsent(10, (id) -> new Product(1, "test", 10));
        System.out.println(result);
        System.out.println(idToProducts.get(10));

        idToProducts.forEach((key, value) -> {
            System.out.println(key + " -> " + value);
        });

    }
}
