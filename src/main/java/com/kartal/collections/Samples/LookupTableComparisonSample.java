package com.kartal.collections.Samples;

import com.kartal.collections.lists.Product;
import com.kartal.collections.maps.IProductLookupTable;
import com.kartal.collections.maps.MapProductLookupTable;
import com.kartal.collections.maps.NaiveProductLookupTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class LookupTableComparisonSample {
    private static final int ITERATIONS = 5;
    private static final int NUMBER_OF_PRODUCTS = 20_000;

    private static final List<Product> products = generateProducts();

    private static List<Product> generateProducts() {
        final List<Product> products = new ArrayList<>();
        final Random weightGenerator = new Random();
        for (int i = 0; i < NUMBER_OF_PRODUCTS; i++) {
            products.add(new Product(i, "Product " + i, 10 + weightGenerator.nextInt(10)));
        }
        Collections.shuffle(products);
        Collections.shuffle(products);
        Collections.shuffle(products);
        return products;
    }

    private static void runLookups(final IProductLookupTable lookupTable) {
        final List<Product> products = LookupTableComparisonSample.products;
        System.out.println("Running lookups with " + lookupTable.getClass().getSimpleName());

        for (int i = 0; i < ITERATIONS; i++) {
            final long startTime = System.currentTimeMillis();

            for (Product product : products) {
                lookupTable.addProduct(product);
            }

            for (Product product : products) {
                final Product result = lookupTable.lookupById(product.getId());

                if (result != product) {
                    throw new IllegalStateException("Lookup table returned wrong result for " + product);
                }
            }

            lookupTable.clear();
            System.out.println(System.currentTimeMillis() - startTime + "ms");
        }
    }

    static void Sample() {
        runLookups(new MapProductLookupTable());
        runLookups(new NaiveProductLookupTable());
    }
}
