package com.kartal.collections.Samples;

import com.kartal.collections.queues.*;

public class QueueSample {

    public static void HelpDeskSample() {
        HelpDesk helpDesk = new HelpDesk();
        helpDesk.enquire(Customer.KADIR, Category.PHONE);
        helpDesk.enquire(Customer.OSMAN, Category.PRINTER);

        helpDesk.processAllEnquiries();
    }

    public static void CategorisedHelpDeskSample() {
        CategorisedHelpDesk helpDesk = new CategorisedHelpDesk();
        helpDesk.enquire(Customer.KADIR, Category.PHONE);
        helpDesk.enquire(Customer.OSMAN, Category.PRINTER);

        helpDesk.processPrinterEnquiry();
        helpDesk.processGeneralEnquiry();
        helpDesk.processPrinterEnquiry();
    }


    public static void PriorityHelpDeskSample() {
        PriorityHelpDesk helpDesk = new PriorityHelpDesk();
        helpDesk.enquire(Customer.KADIR, Category.PHONE);
        helpDesk.enquire(Customer.OSMAN, Category.PRINTER);
        helpDesk.enquire(Customer.SELMAN, Category.TABLET);

        helpDesk.processAllEnquiries();
    }
}
