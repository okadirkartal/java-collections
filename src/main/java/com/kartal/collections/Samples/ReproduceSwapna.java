package com.kartal.collections.Samples;

import java.util.ArrayList;
import java.util.List;

public class ReproduceSwapna {

    static void Sample() {
        List<String> numbers = new ArrayList<>();
        numbers.add("1");
        numbers.add("3");
        numbers.add("2");

        System.err.println("Removing numbers using for loop");
        for (String number : numbers) {
            System.err.println("number = " + number);
            if (number.equals("2"))
                numbers.remove(number);
        }
        System.err.println(numbers);

    }

}
