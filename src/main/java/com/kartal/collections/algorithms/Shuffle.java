package com.kartal.collections.algorithms;

import com.kartal.collections.enums.ProductFixtures;
import com.kartal.collections.lists.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Shuffle {

    public static void Sample() {

        List<Product> products = new ArrayList<>();
        products.add(ProductFixtures.door);
        products.add(ProductFixtures.floorPanel);
        products.add(ProductFixtures.window);

        System.out.println(products);

        Collections.shuffle(products);
        System.out.println(products);

        Collections.shuffle(products);
        System.out.println(products);

        Collections.shuffle(products);
        System.out.println(products);
    }
}
