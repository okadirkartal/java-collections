package com.kartal.collections.queues;

public enum Category {
    PRINTER,
    COMPUTER,
    PHONE,
    TABLET
}
