package com.kartal.collections.queues;

public class Customer {
    public static final Customer SELMAN = new Customer("Selman");

    public static final Customer OSMAN = new Customer("Osman");

    public static final Customer KADIR = new Customer("Kadir");

    private final String name;

    public Customer(final String name) {
        this.name = name;
    }

    public void reply(final String message) {
        System.out.printf("%s: %s\n", name, message);
    }
}
