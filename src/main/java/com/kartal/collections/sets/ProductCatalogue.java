package com.kartal.collections.sets;

import com.kartal.collections.lists.Product;
import com.kartal.collections.lists.Supplier;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class ProductCatalogue implements Iterable<Product> {

    private final Set<Product> products = new TreeSet<Product>(Product.BY_NAME);

    public void isSuppliedBy(Supplier supplier) {
        products.addAll(supplier.getProducts());
    }

    @Override
    public Iterator<Product> iterator() {
        return products.iterator();
    }


}
