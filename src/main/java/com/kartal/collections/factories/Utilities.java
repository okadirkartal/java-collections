package com.kartal.collections.factories;

import com.kartal.collections.enums.ProductFixtures;
import com.kartal.collections.lists.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Utilities {


    public static void UtilitiesSample() {

        Product door = ProductFixtures.door;
        Product floorPanel = ProductFixtures.floorPanel;
        Product window = ProductFixtures.window;

        List<Product> products = new ArrayList<>();
        Collections.addAll(products, door, floorPanel);

        final Product product = Collections.max(products, Product.BY_WEIGHT);
        System.out.println(product);
    }
}
