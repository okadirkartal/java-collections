package com.kartal.collections.maps;

import com.kartal.collections.lists.Product;

import java.util.HashMap;
import java.util.Map;

public class MapProductLookupTable implements IProductLookupTable {

    private final Map<Integer, Product> idToProduct = new HashMap<>();

    @Override
    public void addProduct(Product productToAdd) {
        final int id = productToAdd.getId();
        if (idToProduct.containsKey(id)) {
            throw new IllegalArgumentException("Unable to add product,duplice id for " + productToAdd.getId());
        }
        idToProduct.put(id, productToAdd);
    }

    @Override
    public Product lookupById(int id) {
        return idToProduct.get(id);
    }

    @Override
    public void clear() {
        idToProduct.clear();
    }
}
