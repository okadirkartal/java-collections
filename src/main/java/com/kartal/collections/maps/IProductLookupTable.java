package com.kartal.collections.maps;

import com.kartal.collections.lists.Product;

public interface IProductLookupTable {
    void addProduct(Product productToAdd);

    Product lookupById(int id);

    void clear();
}
