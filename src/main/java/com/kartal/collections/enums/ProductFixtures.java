package com.kartal.collections.enums;

import com.kartal.collections.lists.Product;
import com.kartal.collections.lists.Supplier;

public class ProductFixtures {
    public static Product door = new Product(1, "Wooden Door", 35);
    public static Product floorPanel = new Product(2, "Floor Panel", 25);
    public static Product window = new Product(3, "Glass Window", 10);

    public static Supplier kadir = new Supplier("Kadir's HouseHold Supplies");
    public static Supplier kartal = new Supplier("Kartal's Home Goods");

    static {
        kadir.getProducts().add(door);
        kadir.getProducts().add(floorPanel);

        kartal.getProducts().add(floorPanel);
        kartal.getProducts().add(new Product(1, "Glass Window", 10));
    }
}
